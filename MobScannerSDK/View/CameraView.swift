//
//  CameraView.swift
//  MobScanner
//
//  Created by TechVariable on 31/12/20.
//  Copyright © 2020 TechVariable. All rights reserved.
//

import UIKit
import AVFoundation

public class CameraView: UIView,AVCapturePhotoCaptureDelegate {

    private var session: AVCaptureSession?
    private var stillImageOutput: AVCapturePhotoOutput?
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    private var backCamera : AVCaptureDevice?
    private var flashMode = AVCaptureDevice.FlashMode.auto
    private var delegate: CameraDelegate? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func startCamera(delegate: CameraDelegate){
        
        self.delegate = delegate
        session = AVCaptureSession()
        session!.sessionPreset = AVCaptureSession.Preset.photo
        
        backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            
        if(backCamera == nil){
            
            self.delegate?.onError(errorMessage:"Unable to access back camera!")
            return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera!)
            stillImageOutput = AVCapturePhotoOutput()
            if session!.canAddInput(input) && session!.canAddOutput(stillImageOutput!) {
                session!.addInput(input)
                session!.addOutput(stillImageOutput!)
                
                setupLivePreview()
            }
        }
        catch let error  {
            self.delegate?.onError(errorMessage: "Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
    }
    
    public func stopCamera(){
        session?.stopRunning()
    }
    
    private func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
        videoPreviewLayer!.videoGravity = AVLayerVideoGravity.resizeAspect
        videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        self.layer.addSublayer(videoPreviewLayer!)
        session!.startRunning()
       
        videoPreviewLayer!.frame = self.bounds
    }
    
    public func onResize(newSize: CGSize){
        
        videoPreviewLayer!.frame = CGRect(origin: CGPoint(x: 0,y: 0), size: newSize)
        
        switch UIDevice.current.orientation{
        case .portrait:
            videoPreviewLayer!.connection?.videoOrientation = .portrait
            break
        case .portraitUpsideDown:
            videoPreviewLayer!.connection?.videoOrientation = .portraitUpsideDown
            break
        case .landscapeLeft:
            videoPreviewLayer!.connection?.videoOrientation = .landscapeRight
            break
        case .landscapeRight:
            videoPreviewLayer!.connection?.videoOrientation = .landscapeLeft
            break
        default:
         
            break
        }
        
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if(backCamera == nil){
            
            print("Unable to access back camera!")
            return
        }
        
        let touchPoint = touches.first! as UITouch
        let screenSize = self.bounds.size
        let focusPoint = CGPoint(x: touchPoint.location(in: self).y / screenSize.height, y: 1.0 - touchPoint.location(in: self).x / screenSize.width)

        if let device = backCamera {
            do {
                try device.lockForConfiguration()
                if device.isFocusPointOfInterestSupported {
                    device.focusPointOfInterest = focusPoint
                    device.focusMode = AVCaptureDevice.FocusMode.autoFocus
                }
                if device.isExposurePointOfInterestSupported {
                    device.exposurePointOfInterest = focusPoint
                    device.exposureMode = AVCaptureDevice.ExposureMode.autoExpose
                }
                device.unlockForConfiguration()

            } catch {
                // Handle errors here
                print("Error in auto focus")
            }
        }
    }
    
    public func capture(){
        
        if(backCamera == nil){
            
            print("Unable to access back camera!")
            return
        }
        let settings = AVCapturePhotoSettings()

        if backCamera!.hasFlash {
            settings.flashMode = flashMode
        }
        
        stillImageOutput?.capturePhoto(with: settings, delegate: self)
    }
    
    public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard let imageData = photo.fileDataRepresentation()
           else { return }
        
        let image = UIImage(data: imageData)
        
        self.delegate?.onImageCaptured(image: image)
    }
    
    public func updateFlashMode(flasMode: FlashModes){
        
        switch flasMode {
        case .on:
            self.flashMode = .on
            break
        case .off:
            self.flashMode = .off
            break
        default:
            self.flashMode = .auto
            break
        }
    }
}
