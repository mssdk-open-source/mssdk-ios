//
//  FilterOptionList.swift
//  MobScanner
//
//  Created by TechVariable on 23/12/20.
//  Copyright © 2020 TechVariable. All rights reserved.
//

import UIKit

public protocol FilterOptionDelegate{
    
    func onFilterSelected(filterName: String, filteredImage: UIImage)
    func getOriginalImage() -> UIImage
}

public class FilterOptionList: UIView,CardViewListDelegete {

    private var cardViewList: CardViewList!
   
    private var delegate: FilterOptionDelegate? = nil
    
    public override init(frame: CGRect) {
        
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func cardView(_ scrollView: UIScrollView, didSelectCardView cardView: UIView, identifierCards identifier: String, index: Int) {
        
        let selectedCard = cardView as! FilterCardView
        for card in cardViewList.getAllViews(){
            
            let fCard = card as! FilterCardView
            if(fCard.isSelected){
                fCard.isSelected = false
                break
            }
        }
        
        selectedCard.isSelected = true
        
        if(delegate != nil){
            
            MSSDKCore.applyFilter(filterType: selectedCard.filterName, image: (delegate?.getOriginalImage())!, callback: {image in
                
                self.delegate!.onFilterSelected(filterName: selectedCard.filterName.rawValue, filteredImage: image!)
            })
        }
    }
    
    public func setUp(delegate: FilterOptionDelegate){
        
        self.delegate = delegate
        self.cardViewList = CardViewList()
        self.cardViewList.delegete = self
        
        MSSDKCore.applyFilter(filterType: FilterTypes.Auto, image: (delegate.getOriginalImage()), callback: {image in
            
            self.delegate!.onFilterSelected(filterName: FilterTypes.Auto.rawValue, filteredImage: image!)
        })
        
        // Create CardView List from UIView
        var cardViews = [UIView]()
        let previewImg = Utils.resizeImage(image: delegate.getOriginalImage(), newHeight: 300)
        for filterType in FilterTypes.allCases{
            
            let card = FilterCardView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
            card.filterName = filterType
            if(previewImg != nil){
                card.setUp(previewImg: previewImg!)
            }
            cardViews.append(card)
        }
        
        self.cardViewList.animationScroll = .scaleBounce
        self.cardViewList.isClickable = true
        self.cardViewList.clickAnimation = .none
        self.cardViewList.cardSizeType = .autoSize
        self.cardViewList.grid = 1
        self.cardViewList.generateCardViewList(containerView: self, views: cardViews, listType: .horizontal, identifier: "CardWithUIView")
        
        self.backgroundColor = UIColor.black
    }

}
