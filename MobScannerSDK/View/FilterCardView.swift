//
//  FilterCardView.swift
//  MobScanner
//
//  Created by TechVariable on 23/12/20.
//  Copyright © 2020 TechVariable. All rights reserved.
//

import UIKit

public class FilterCardView: UIView {

    let txtField : UITextField = UITextField()
    var checkBox: BEMCheckBox? = nil
    let preview = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public override func draw(_ rect: CGRect) {
        checkBox?.isHidden = !isSelected
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public var filterName: FilterTypes = FilterTypes.Auto
    
    public var isSelected: Bool = false {
        
        didSet{
            
            self.setNeedsDisplay()
        }
    }
    
    
    public func setUp(previewImg: UIImage){
        
        txtField.frame = CGRect(x: 0, y: self.frame.height - 25, width: self.frame.width,height: 30)
       
        txtField.backgroundColor = UIColor.init(argb: 0x90000000)
        txtField.text = filterName.rawValue
        txtField.textColor = UIColor.white
        txtField.textAlignment = .center
        txtField.layer.zPosition = 0
        
        preview.frame = CGRect(x: 0, y: 2, width: self.frame.width,height: self.frame.height)
        
        MSSDKCore.applyFilter(filterType: filterName,image: previewImg, callback:{image in
            
            self.preview.image = image
        })
        
        self.addSubview(preview)
        self.addSubview(txtField)
        
        checkBox = BEMCheckBox(frame: CGRect(x: self.frame.width - 30,y: 5,width: 25,height: 25))
        checkBox!.onAnimationType = BEMCheckBox.AnimationType.fill
        checkBox!.on = true
        checkBox!.onCheckColor = UIColor.white
        checkBox!.onFillColor = MobScannerSDK.INSTANCE.colors.primaryColor
        checkBox!.onTintColor = UIColor.white
        isSelected = (filterName.rawValue == FilterTypes.Auto.rawValue)
        checkBox!.isHidden = isSelected
       
        self.addSubview(checkBox!)
        self.backgroundColor = UIColor.black
    }
}
