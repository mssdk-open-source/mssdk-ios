//
//  BlurLoader.swift
//  MobScannerSDK
//
//  Created by Bharat H on 03/01/21.
//  Copyright © 2021 TechVariable. All rights reserved.
//

import Foundation

extension UIView {
    func showBlurLoader() {
        let blurLoader = BlurLoader(frame: frame)
        self.addSubview(blurLoader)
    }

    func removeBluerLoader() {
        if let blurLoader = subviews.first(where: { $0 is BlurLoader }) {
            blurLoader.removeFromSuperview()
        }
    }
}


class BlurLoader: UIView {

    var blurEffectView: UIVisualEffectView?

    override init(frame: CGRect) {
        let blurEffect = UIBlurEffect(style: .prominent)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = frame
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.blurEffectView = blurEffectView
        super.init(frame: frame)
        addSubview(blurEffectView)
        addLoader()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addLoader() {
        guard let blurEffectView = blurEffectView else { return }
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.color = MobScannerSDK.INSTANCE.colors.primaryColor
        let label = UILabel()
        label.text = "Please wait..."
        label.textColor = MobScannerSDK.INSTANCE.colors.label
        blurEffectView.contentView.addSubview(activityIndicator)
        activityIndicator.center = blurEffectView.contentView.center
        label.frame = CGRect(x: 0, y: activityIndicator.frame.maxY, width: self.frame.width, height: 50)
        label.textAlignment = .center
        blurEffectView.contentView.addSubview(label)
        
        activityIndicator.startAnimating()
    }
}
