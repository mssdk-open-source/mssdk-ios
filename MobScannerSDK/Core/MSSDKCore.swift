//
//  MSSDKCore.swift
//  MobScanner
//
//  Created by TechVariable on 30/12/20.
//  Copyright © 2020 TechVariable. All rights reserved.
//

import UIKit


public class MSSDKCore {
    
    public static func cropUsingCIFilter(image: UIImage,corners: [CGPoint]) -> UIImage?{
        
        guard let ciImage = CIImage(image: image) else {
            return nil
        }
        
        let cgOrientation = CGImagePropertyOrientation(rawValue: UInt32(image.imageOrientation.rawValue))
        let orientedImage = ciImage.oriented(forExifOrientation: Int32(cgOrientation!.rawValue))
       
        let inputBottomLeft =  corners[3]
                               let inputTopLeft = corners[0]
                               let inputTopRight = corners[1]
                               let inputBottomRight = corners[2]
        
        
        let filteredImage = orientedImage.applyingFilter("CIPerspectiveCorrection", parameters: [
            "inputTopLeft": CIVector(cgPoint: inputBottomLeft),
            "inputTopRight": CIVector(cgPoint: inputBottomRight),
            "inputBottomLeft": CIVector(cgPoint: inputTopLeft),
            "inputBottomRight": CIVector(cgPoint: inputTopRight)
        ])
        
        if let cgImage = CIContext(options: nil).createCGImage(filteredImage, from: filteredImage.extent) {
            return UIImage(cgImage: cgImage).rotate(degrees: 180)
        }
      
        return nil
        
    }
    
    public static func getTransformedImage(newWidth: CGFloat,newHeight: CGFloat, origImage: UIImage,corners: [CGPoint],size: CGSize,callback: @escaping ((UIImage?) -> ())){
        
        checkInitialization()
        
        DispatchQueue.background(background: { ()->(UIImage?) in
            
            var copyCorner = corners
            let croppedImg = OpenCVWrapper.getTransformedImage(newWidth, newHeight, origImage, &copyCorner, size)
         
            let isWhite = OpenCVWrapper.isAllWhitePixels(croppedImg)
         
            if(isWhite){
              
              return cropUsingCIFilter(image: origImage.rotate(degrees: 180)!, corners: corners)
            }
            
            return croppedImg
            
        }, completion: { (image) ->() in
            
            callback(image)
        })
    }
    
    static func flattenImage(image: CIImage, topLeft: CGPoint, topRight: CGPoint,bottomLeft: CGPoint, bottomRight: CGPoint) -> CIImage {

        return image.applyingFilter("CIPerspectiveCorrection", parameters: [

            "inputTopLeft": CIVector(cgPoint: topLeft),
            "inputTopRight": CIVector(cgPoint: topRight),
            "inputBottomLeft": CIVector(cgPoint: bottomLeft),
            "inputBottomRight": CIVector(cgPoint: bottomRight)


            ])

    }
    
    public static func getLargestSquarePoints(image: UIImage, frameSize: CGSize,callback: @escaping ((NSMutableArray?) -> ())){
        
        checkInitialization()
        DispatchQueue.background(background: { ()->(NSMutableArray) in
                
            return OpenCVWrapper.getLargestSquarePoints(image, frameSize)
                
            }, completion: { (points) ->() in
                
                callback(points)
            })
    }
    
    private static func checkInitialization(){
        
       /** if(!MobScannerSDK.isInitialized()){
            fatalError("SDK Not intialized")
        }**/
    }
    
    public static func applyFilter(filterType: FilterTypes, image: UIImage,callback: @escaping ((UIImage?) -> ())){
        
        checkInitialization()
        DispatchQueue.background(background: { ()->(UIImage) in
            
            switch(filterType){
                
            case .Auto:
                return OpenCVWrapper.getAutoImage(image)
                
            case .Magic:
                return OpenCVWrapper.getMagicImage(image)
                
            case .Gray:
                return OpenCVWrapper.getGrayImage(image)
                
            case .BW1:
                return OpenCVWrapper.applyAdaptiveGaussian(image, 9)
                
            case .BW2:
                return OpenCVWrapper.applyAdaptiveMean(image, 9)

            case .none:
                return image
            }
            
        }, completion: { (image) ->() in
            
            callback(image)
        })
    }
}
