//
//  SDInitializer.swift
//  MobScannerSDK
//
//  Created by TechVariable on 06/01/21.
//  Copyright © 2021 TechVariable. All rights reserved.
//

import UIKit

class SDKInitializer {

    private final let key256   = "B?E(H+MbQeThVmYq3t6w9z$C&F)J@NcR"   // 32 bytes for AES256
    private final let iv       = "A?D(G+KbPeShVmYq"                   // 16 bytes for AES128
    private var isInitialized = false
    
    func isSDKIntialized() -> Bool{
        
        return isInitialized
    }
    
    func initialize(uuid: String,apiKey: String,sdkInitDelegate: SDKInitDelegate){
        
        do{
            let bundle = Bundle(for: SDKInitializer.self)
            
             let bundlePath = bundle.path(forResource: "auth",
                                                     ofType: "bin")
             let val =  try String(contentsOfFile: bundlePath!)
            
             let aes = AES(key: key256, iv: iv)
             let jsonData = (aes?.decrypt(data: Data(base64Encoded: val)))
             
            if(jsonData != nil){
                let auths: [ApiAuth] = try! JSONDecoder().decode([ApiAuth].self, from: (jsonData?.data(using: .utf8))!)
                
                for auth in auths {
                    
                    if(auth.accountId.elementsEqual(uuid)){
                        
                        let apps = auth.apps
                        
                        for app in apps {
                            
                            if(app.apiKey.elementsEqual(apiKey)){
                                
                                let bundleId = Bundle.main.bundleIdentifier
                                
                                if(bundleId != nil){
                                    if(bundleId!.elementsEqual(app.bundleId)){
                                     
                                        isInitialized = true
                                        sdkInitDelegate.onInitSuccess()
                                        return
                                    }
                                }
                                
                                break
                            }
                        }
                        
                        break
                    }
                }
                   
            }
            
         }catch{
             
            print("Error \(error)")
            sdkInitDelegate.onInitError(message: "Error raised \(error)")
            return
         }
        
        sdkInitDelegate.onInitError(message: "SDK initalization failed. Check API key or UUID provided.")
    }
}

private struct ApiAuth: Decodable {
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        accountId = try container.decode(String.self, forKey: .accountId)
        apps = try container.decode([App].self, forKey: .apps)
    }
    
    enum CodingKeys: String, CodingKey {
        case accountId
        case apps
        case bundleId
        case vendor
        case apiKey
    }
    
    let accountId: String
    let apps: [App]
}

private struct App: Decodable{
    
    let bundleId: String
    let vendor: String
    let apiKey: String
}
