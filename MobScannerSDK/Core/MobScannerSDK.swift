//
//  ColorConfig.swift
//  MobScannerSDK
//
//  Created by TechVariable on 03/01/21.
//  Copyright © 2021 TechVariable. All rights reserved.
//

import Foundation


public class MobScannerSDK{
    
    private init() { }
    
    static let INSTANCE = MobScannerSDK()
    private static let sdkInitializer =  SDKInitializer()
    
    public static func setTheme(theme: SDKColors){
        
        INSTANCE.self.colors = theme
    }
    
    public static func isInitialized()-> Bool{
        
        return true
        //TODO: to be removed
       // return sdkInitializer.isSDKIntialized()
    }
    
    public static func initialize(uuid: String, apiKey: String, initCallBack: SDKInitDelegate ){
        
        //TODO: to be removed
       // sdkInitializer.initialize(uuid: uuid,apiKey: apiKey, sdkInitDelegate: initCallBack)
    }
    
    public static func getSDKVersion()-> String{
        
         let bundle = Bundle(for: MobScannerSDK.self)
        return bundle.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }
    
    /// The colors to apply to the interface elements.
    var colors: SDKColors = SDKDefaultColors()
}
