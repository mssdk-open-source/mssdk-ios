//
//  ColorConfig.swift
//  MobScannerSDK
//
//  Created by Bharat H on 03/01/21.
//  Copyright © 2021 TechVaribale. All rights reserved.
//

import Foundation

/// Defines colors that will be applied to the UI elements.
public protocol SDKColors {

    /// This is the primary action color used for tinting buttons like the Back, Done and other Button color
    var primaryColor: UIColor { get }

    /// The main color for text labels.
    var label: UIColor { get }
    
    /// Used as the background color for nav bars
    var navBackgroundColor: UIColor { get }
}

/// Defines the default colors .
public struct SDKDefaultColors: SDKColors {
    
    public var navBackgroundColor: UIColor = {
        if #available(iOS 13.0, *) {
            return .systemBackground
        } else {
            return .white
        }
    }()

    public var primaryColor: UIColor = {
        if #available(iOS 13.0, *) {
            return .link
        } else {
            return UIColor(red: 0.33, green: 0.63, blue: 0.97, alpha: 1.00)
        }
    }()

    public let label: UIColor = {
        if #available(iOS 13.0, *) {
            return .label
        } else {
            return .black
        }
    }()
}
