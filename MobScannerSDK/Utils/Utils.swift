//
//  Utils.swift
//  MobScanner
//
//  Created by TechVariable on 23/08/20.
//  Copyright © 2020 TechVariable. All rights reserved.
//

import UIKit

class Utils: NSObject {

    public static func getTempFolder()->URL?{
        
        let tempFolderPath = NSTemporaryDirectory()
        return URL(fileURLWithPath: tempFolderPath)
    }
    
    public static func getScanFolderName()->String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yy-HH:mm:ss"
        let dateStr = dateFormatter.string(from: Date())
        return "Scan "+dateStr
    }
    
    public static func getFolder(folderName: String)->URL?{
        return URL.createFolder(folderName: "MobScanner/"+folderName)
    }
    
    public static func saveCameraImage(image: UIImage)->String?{
        
        let tempFolder = getTempFolder()
        
        if(tempFolder != nil){
            return saveImage(folder: tempFolder!, image: image)
        }else{
            let fileName = "Image_"+String(Int.random(in: 100..<10000))+".jpg"
            return saveImage(imageName: fileName, image: image)
        }
    }
    
    public static func saveImage(folder: URL, image: UIImage)->String?{
        
        var resourcesContent: [URL] {
            (try? FileManager.default.contentsOfDirectory(at: folder, includingPropertiesForKeys: nil)) ?? []
        }
        let count = resourcesContent.count
        
        let fileName = "Image_"+String((count+1))+String(Int.random(in: 100..<5000))+".jpg"
        let fileURL = folder.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1.0) else { return nil }
        
        do {
            try data.write(to: fileURL)
            return fileURL.path
        } catch let error {
            print("error saving file with error", error)
            return nil
        }
    }
    
    public static func clearTempFolder() {
        let fileManager = FileManager.default
        let tempFolderPath = NSTemporaryDirectory()

        do {
            let filePaths = try fileManager.contentsOfDirectory(atPath: tempFolderPath)
            for filePath in filePaths {
                try fileManager.removeItem(atPath: NSTemporaryDirectory() + filePath)
            }
        } catch let error as NSError {
            print("Could not clear temp folder: \(error.debugDescription)")
        }
    }
    
    public static func saveImage(imageName: String, image: UIImage)->String? {

     guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil}

        let fileName = imageName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1.0) else { return nil }

        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }

        }

        do {
            try data.write(to: fileURL)
            return fileURL.path
        } catch let error {
            print("error saving file with error", error)
            return nil
        }

    }

   public static func loadImageFromDiskWith(fileName: String) -> UIImage? {

      let documentDirectory = FileManager.SearchPathDirectory.documentDirectory

        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)

        if let dirPath = paths.first {
           // let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
           
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image

        }

        return nil
    }

    public static func resizeImage(image: UIImage, newHeight: CGFloat) -> UIImage? {

        let scale = newHeight / image.size.height
        let newWidth = image.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
    
    public static func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {

        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
    public static func createNavBackButton(showBackText: Bool = true, tintColor: UIColor = MobScannerSDK.INSTANCE.colors.primaryColor) -> UIButton{
        
        let backButton = UIButton(type: .custom)
        
        if #available(iOS 13.0, *) {
            let config = UIImage.SymbolConfiguration(pointSize: 25.0, weight: .medium, scale: .medium)
            let image = UIImage(systemName: "chevron.left", withConfiguration: config)
            backButton.setImage(image, for: .normal)
            
        } else {

            let bckImg = UIImage(named:"chevron-left.png",in: Bundle(for: Utils.self),compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
            let targetSize = CGSize(width: 20, height: 20)

            let renderer = UIGraphicsImageRenderer(size: targetSize)
            let scaledImage = renderer.image { _ in
                bckImg?.draw(in: CGRect(origin: .zero, size: targetSize))
            }
            backButton.setImage(scaledImage.withRenderingMode(.alwaysTemplate), for: .normal)
            
        }
        
        // create back button
        
        if(showBackText){
            backButton.setTitle("Back", for: .normal)
        }
        backButton.setTitleColor(tintColor, for: .normal)
        backButton.tintColor = tintColor
        return backButton
    }
}
