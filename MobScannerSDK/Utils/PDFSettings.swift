//
//  PDFSettings.swift
//  MobScannerSDK
//
//  Created by TechVariable on 04/01/21.
//  Copyright © 2021 TechVariable. All rights reserved.
//

import UIKit

public enum PDFSize{
    
    case A4,A3,LETTER
}

public class PDFSettings {

    public init() {
    }
    
    public var pdfSize = PDFSize.A4
    public var applyMargin = false
    public var imageQuality = 0.9
    
}
