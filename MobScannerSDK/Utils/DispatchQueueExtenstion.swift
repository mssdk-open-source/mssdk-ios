//
//  DispatchQueueExtenstion.swift
//  MobScanner
//
//  Created by TechVariable on 30/12/20.
//  Copyright © 2020 TechVariable. All rights reserved.
//

import UIKit

public extension DispatchQueue {

    static func background<T>(background: (()->T?)? = nil, completion: ((T?) -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            let retVal = background?()
            if let completion = completion {
                DispatchQueue.main.async {
                    completion(retVal)
                }
            }
        }
    }

}
