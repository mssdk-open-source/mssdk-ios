//
//  FilterTypes.swift
//  MobScanner
//
//  Created by TechVariable on 30/12/20.
//  Copyright © 2020 TechVariable. All rights reserved.
//

import Foundation

public enum FilterTypes: String,CaseIterable{
    
    case none = "None"
    case Auto = "Auto"
    case Magic = "Magic"
    case Gray = "Gray"
    case BW1 = "B&W1"
    case BW2 = "B&W2"
}
