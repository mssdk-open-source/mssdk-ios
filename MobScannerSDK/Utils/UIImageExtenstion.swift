//
//  UIImageExtenstion.swift
//  MobScanner
//
//  Created by TechVariable on 30/12/20.
//  Copyright © 2020 TechVariable. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    
    func rotate(degrees: Float) -> UIImage? {
        let _: (CGFloat) -> CGFloat = {
            return $0 * (180.0 / .pi)
        }
        let degreesToRadians: (CGFloat) -> CGFloat = {
            return $0 / 180.0 * .pi
        }

        let radians = degreesToRadians(CGFloat(degrees))
       var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!

        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage

    }
    
    func resize(targetSize: CGSize, targetQuality: CGFloat = 1.0) -> UIImage {
        // Determine the scale factor that preserves aspect ratio
        let widthRatio = targetSize.width / size.width
        let heightRatio = targetSize.height / size.height
   
        let jpegData = self.jpegData(compressionQuality: 1.0)
        let jpegSize: Int = jpegData?.count ?? 0
        print("Original size of jpeg image in KB: %f ", Double(jpegSize) / 1024.0)
        
        let scaleFactor = min(widthRatio, heightRatio)
        
        // Compute the new image size that preserves aspect ratio
        let scaledImageSize = CGSize(
            width: size.width * scaleFactor,
            height: size.height * scaleFactor
        )

        // Draw and return the resized UIImage
        let renderer = UIGraphicsImageRenderer(
            size: scaledImageSize
        )

        let scaledImage = renderer.image { _ in
            self.draw(in: CGRect(
                origin: .zero,
                size: scaledImageSize
            ))
        }
        
        var quality = targetQuality
        
        if(targetQuality > 1.0 || targetQuality < 0){
            quality = 1.0
        }
        
        let data = scaledImage.jpegData(compressionQuality: quality)
        
        if(data == nil){
            return scaledImage
        }
        let uiImage = UIImage(data: data!)
      
        if(uiImage == nil){
            return scaledImage
        }
        let jpegDataResized = uiImage!.jpegData(compressionQuality: 1.0)
        let jpegSizeResized: Int = jpegDataResized?.count ?? 0
        print("Resized size of jpeg image in KB: %f ", Double(jpegSizeResized) / 1024.0)
        return uiImage!
    }
}
