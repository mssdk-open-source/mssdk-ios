//
//  Img2Pdf.swift
//  MobScannerSDK
//
//  Created by Bharat H on 04/01/21.
//  Copyright © 2021 TechVariable. All rights reserved.
//

import UIKit
import PDFKit

public class Img2Pdf {
    
    public static func createPDF(folderPath: URL, jpegFiles: [String],outputFileName: String,outputJpegQuality: Float = 0.9,callback: @escaping (((URL?)?) -> ())){
        
        DispatchQueue.background(background: { ()->(URL?) in
            
             let docURL = folderPath.appendingPathComponent(outputFileName)
            return createPDF(docURL: docURL,jpegFiles: jpegFiles,quality: outputJpegQuality)
         }, completion: { (url) ->() in
             
             callback(url)
         })
        
    }
    
    private static func createPDF(docURL: URL, jpegFiles: [String], quality: Float = 0.9)->URL?{
        
        let pageSize = CGSize(width: 800,height: 1131)
        // let aspectRatio = pageSize.height/pageSize.width
        // let margin = (pdfSettings.applyMargin) ? CGFloat(40) : CGFloat(0)
        //   let marginWithAr = (margin/aspectRatio)
        //   let maxWidth = pageSize.width + marginWithAr
        //   let maxHeight = pageSize.height + margin
        
        let pdfDocument = PDFDocument()
        
        for(index,imagePath) in jpegFiles.enumerated(){
            
            let image = UIImage(contentsOfFile: imagePath)
            if(image != nil){
                
                let data:Data = image!.jpegData(compressionQuality: CGFloat(quality))!
                let compressedImg = UIImage(data: data)
                
                if((compressedImg?.size.width)! > pageSize.width){
                    let resizedImg = compressedImg?.resize(toWidth: pageSize.width)
                    let pdfPage = PDFPage(image: resizedImg!)
                    // Insert the PDF page into your document
                    pdfDocument.insert(pdfPage!, at: index)
                }else{
                    let pdfPage = PDFPage(image: image!)
                    // Insert the PDF page into your document
                    pdfDocument.insert(pdfPage!, at: index)
                }
            }
        }
        
        let data = pdfDocument.dataRepresentation()
        
        do{
            try data?.write(to: docURL)
        }catch(let error)
        {
            print("error is \(error.localizedDescription)")
            return nil
        }
        
        return docURL
        
    }
    
    
    public static func createPDF(jpegFiles: [String],outputFileName: String,outputJpegQuality: Float = 0.9,callback: @escaping (((URL?)?) -> ())){
        
        DispatchQueue.background(background: { ()->(URL?) in
           
            let documentDirectory = Utils.getFolder(folderName: "Pdf")
            let docURL = documentDirectory!.appendingPathComponent(outputFileName)
            return createPDF(docURL: docURL,jpegFiles: jpegFiles, quality: outputJpegQuality)
        }, completion: { (url) ->() in
            
            callback(url)
        })
       
    }
}
extension UIImage{
    func resize(toWidth width: CGFloat) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
}
