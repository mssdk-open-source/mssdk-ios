//
//  MSSDKProtocols.swift
//  MobScannerSDK
//
//  Created by TechVariable on 02/01/21.
//  Copyright © 2021 TechVariable. All rights reserved.
//

import Foundation

public protocol InputImagePickerDelegate{
    
    func onInputImagePicked(images: [String])
}

public protocol DocumentReadyDelegate{

    func onDocumentReady(outputImgFiles: [String])
    func onUserCancelled()
}

public protocol SDKInitDelegate{
    
    func onInitSuccess()
    func onInitError(message: String)
}

public enum SDKErrors: LocalizedError{
    
    case NOT_INITIALIZED
}

public protocol CameraDelegate {
    
    func onImageCaptured(image: UIImage?)
    func onError(errorMessage: String)
}

public enum FlashModes{
    
    case auto,on,off
}
