//
//  OpenCVWrapper.m
//  MobScanner
//
//  Created by TechVariable on 14/12/20.
//  Copyright © 2020 TechVariable. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import "OpenCVWrapper.h"
#import <UIKit/UIKit.h>
#undef NO
#import <opencv2/imgcodecs/ios.h>
#import <opencv2/stitching.hpp>
#include "opencv2/imgproc/imgproc_c.h"
using namespace std;


@implementation OpenCVWrapper
+ (NSString *)openCVVersionString {
    return [NSString stringWithFormat:@"OpenCV Version %s",  CV_VERSION];
}

+(NSMutableArray *) getLargestSquarePoints: (UIImage *) image : (CGSize) size{
    
    
    cv::Mat imageMat;
    
    
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to backing data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    imageMat = cvMat;
    
    cv::resize(imageMat, imageMat, cvSize(size.width, size.height));
    
    //    UIImageToMat(image, imageMat);
    
    
    std::vector<std::vector<cv::Point> >rectangle;
    std::vector<cv::Point> largestRectangle;
    
    getRectangles(imageMat, rectangle);
    getlargestRectangle(rectangle, largestRectangle);
    
    if (largestRectangle.size() == 4)
    {
        
        //        Thanks to: https://stackoverflow.com/questions/20395547/sorting-an-array-of-x-and-y-vertice-points-ios-objective-c/20399468#20399468
        
        NSArray *points = [NSArray array];
        points = @[
            [NSValue valueWithCGPoint:(CGPoint){(CGFloat)largestRectangle[0].x, (CGFloat)largestRectangle[0].y}],
            [NSValue valueWithCGPoint:(CGPoint){(CGFloat)largestRectangle[1].x, (CGFloat)largestRectangle[1].y}],
            [NSValue valueWithCGPoint:(CGPoint){(CGFloat)largestRectangle[2].x, (CGFloat)largestRectangle[2].y}],
            [NSValue valueWithCGPoint:(CGPoint){(CGFloat)largestRectangle[3].x, (CGFloat)largestRectangle[3].y}]                            ];
        
        CGPoint min = [points[0] CGPointValue];
        CGPoint max = min;
        for (NSValue *value in points) {
            CGPoint point = [value CGPointValue];
            min.x = fminf(point.x, min.x);
            min.y = fminf(point.y, min.y);
            max.x = fmaxf(point.x, max.x);
            max.y = fmaxf(point.y, max.y);
        }
        
        CGPoint center = {
            0.5f * (min.x + max.x),
            0.5f * (min.y + max.y),
        };
        
        NSLog(@"center: %@", NSStringFromCGPoint(center));
        
        NSNumber *(^angleFromPoint)(id) = ^(NSValue *value){
            CGPoint point = [value CGPointValue];
            CGFloat theta = atan2f(point.y - center.y, point.x - center.x);
            CGFloat angle = fmodf(M_PI - M_PI_4 + theta, 2 * M_PI);
            return @(angle);
        };
        
        NSArray *sortedPoints = [points sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            return [angleFromPoint(a) compare:angleFromPoint(b)];
        }];
        
        NSLog(@"sorted points: %@", sortedPoints);
        
        NSMutableArray *squarePoints = [[NSMutableArray alloc] init];
        [squarePoints addObject: [sortedPoints objectAtIndex:0]];
        [squarePoints addObject: [sortedPoints objectAtIndex:1]];
        [squarePoints addObject: [sortedPoints objectAtIndex:2]];
        [squarePoints addObject: [sortedPoints objectAtIndex:3]];
        imageMat.release();
        
        return squarePoints;
        
        
    }
    else{
        imageMat.release();
        return nil;
    }
    
}

+(UIImage *) getGrayImage: (UIImage *) origImage{
    
    cv::Mat imageMat;
    UIImageToMat(origImage,imageMat);
    if(imageMat.channels() == 1) return origImage;
    
    cv::Mat greyMat;
    cv::cvtColor(imageMat, greyMat, cv::COLOR_BGR2GRAY);
    
    return MatToUIImage(greyMat);
}

cv::Mat getMorphologicalEx(cv::Mat& image){
    
    cv::Mat tempDst;
    cv::medianBlur(image, tempDst, 5);
    
    cv::Mat mrphDst;
    
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(15,15));
    
    cv::morphologyEx(tempDst,mrphDst,cv::MORPH_CLOSE,kernel,cv::Point(-1,-1),1,cv::BORDER_REFLECT101 );
    
    return mrphDst;
}

+(UIImage *) getMagicImage: (UIImage *) origImage{
    
    cv::Mat imageMat;
    UIImageToMat(origImage,imageMat);
    cv::Mat morphMat = getMorphologicalEx(imageMat);
    
    cv::Mat dest(imageMat.size(), CV_8UC3);
    
    cv::divide(imageMat, morphMat, dest,255);
    
    return MatToUIImage(getAutoImage(dest));
}

+(UIImage *) getAutoImage:(UIImage *)origImage{
    
    cv::Mat imageMat;
    UIImageToMat(origImage,imageMat);
    
    return MatToUIImage(getAutoImage(imageMat));
}

cv::Mat getAutoImage(cv::Mat inputImg){
    
    AlphaBetaValue alphaBetaVal = getAlphaBeta(inputImg);
    
    cv::Mat dest;
    inputImg.convertTo(dest, -1, alphaBetaVal.alpha, alphaBetaVal.beta);
    
    return dest;
}



+(UIImage *) applyAdaptiveMean: (UIImage *)origImage : (int) blockSize{
    
    cv::Mat imageMat;
    UIImageToMat(origImage,imageMat);
    
    cv::Mat gray;
    cv::cvtColor(imageMat, gray, cv::COLOR_BGR2GRAY);
    
    cv::Mat dst;
    cv::adaptiveThreshold(gray,dst,255,cv::ADAPTIVE_THRESH_MEAN_C,cv::THRESH_BINARY,blockSize,10);
    return MatToUIImage(dst);
}

+(Boolean) isAllWhitePixels:(UIImage *)origImage{
    
    cv::Mat imageMat;
    UIImageToMat(origImage,imageMat);
    
    cv::Mat gray;
    cv::cvtColor(imageMat, gray, cv::COLOR_BGR2GRAY);
    
    cv::Mat dst;
    cv::threshold(gray, dst, 0, 255, cv::THRESH_BINARY+cv::THRESH_OTSU);
    
    int pixels = cv::countNonZero(dst);
    
    Boolean sts =  (pixels == imageMat.rows*imageMat.cols);
    return sts;
}

+(UIImage *) applyAdaptiveGaussian: (UIImage *)origImage : (int) blockSize{
    
    cv::Mat imageMat;
    UIImageToMat(origImage,imageMat);
    
    cv::Mat gray;
    cv::cvtColor(imageMat, gray, cv::COLOR_BGR2GRAY);
    
    cv::Mat dst;
    cv::adaptiveThreshold(gray,dst,255,cv::ADAPTIVE_THRESH_GAUSSIAN_C,cv::THRESH_BINARY,blockSize,10);
    return MatToUIImage(dst);
    
}

struct AlphaBetaValue{
    float alpha = 1.9f;
    float beta = -80;
};

AlphaBetaValue getAlphaBeta(cv::Mat& imageMat){
    
    struct AlphaBetaValue alphaBetaVal;
    
    try{
        cv::Mat gray;
        cv::cvtColor(imageMat, gray, cv::COLOR_BGR2GRAY);
        
        cv::Mat histogram;
        
        //Calculate grayscale histogram
        
        int channels[] = {0};
        int histSize[] = {256};
        float intensityRanges[] = { 0, 256 };
        const float* ranges[] = { intensityRanges };
        cv::calcHist( &gray, 1, channels, cv::noArray(),histogram,1,histSize,ranges, true, false);
        
        // Calculate cumulative distribution from the histogram
        float accumulator[256] = {};
        accumulator[0] = histogram.at<float>(0, 0);
        
        for( int i = 1; i < 256; i++ )
        {
            accumulator[i] = (float) (accumulator[i -1] + histogram.at<float>(i,0));
        }
        
        float maximum = accumulator[255];
        double clip_hist_percent = (maximum/100.0);
        
        clip_hist_percent /= 2.0;
        
        //Locate left cut
        int minimum_gray = 0;
        while (accumulator[minimum_gray] < clip_hist_percent)
            minimum_gray += 1;
        
        // Locate right cut
        int maximum_gray = 256 -1;
        while(accumulator[maximum_gray] >= (maximum - clip_hist_percent))
            maximum_gray -= 1;
        
        //Calculate alpha and beta values
        float alpha = (float) 255 / (maximum_gray - minimum_gray);
        float beta = -minimum_gray * alpha;
        
        
        alphaBetaVal.alpha = alpha;
        alphaBetaVal.beta = beta;
        
    }
    catch (exception e){
    }
    
    return alphaBetaVal;
}
// http://stackoverflow.com/questions/8667818/opencv-c-obj-c-detecting-a-sheet-of-paper-square-detection
void getRectangles(cv::Mat& image, std::vector<std::vector<cv::Point> >&rectangles) {
    
    // blur will enhance edge detection
    
    cv::Mat blurred(image);
    GaussianBlur(image, blurred, cvSize(11,11), 0);
    
    cv::Mat gray0(blurred.size(), CV_8U), gray;
    std::vector<std::vector<cv::Point> > contours;
    
    // find squares in every color plane of the image
    for (int c = 0; c < 3; c++)
    {
        int ch[] = {c, 0};
        mixChannels(&blurred, 1, &gray0, 1, ch, 1);
        
        // try several threshold levels
        const int threshold_level = 2;
        for (int l = 0; l < threshold_level; l++)
        {
            // Use Canny instead of zero threshold level!
            // Canny helps to catch squares with gradient shading
            if (l == 0)
            {
                Canny(gray0, gray, 10, 20, 3); //
                //                Canny(gray0, gray, 0, 50, 5);
                
                // Dilate helps to remove potential holes between edge segments
                dilate(gray, gray, cv::Mat(), cv::Point(-1,-1));
            }
            else
            {
                gray = gray0 >= (l+1) * 255 / threshold_level;
            }
            
            // Find contours and store them in a list
            findContours(gray, contours, CV_RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
            
            // Test contours
            std::vector<cv::Point> approx;
            for (size_t i = 0; i < contours.size(); i++)
            {
                // approximate contour with accuracy proportional
                // to the contour perimeter
                approxPolyDP(cv::Mat(contours[i]), approx, arcLength(cv::Mat(contours[i]), true)*0.02, true);
                
                // Note: absolute value of an area is used because
                // area may be positive or negative - in accordance with the
                // contour orientation
                if (approx.size() == 4 &&
                    fabs(contourArea(cv::Mat(approx))) > 1000 &&
                    isContourConvex(cv::Mat(approx)))
                {
                    double maxCosine = 0;
                    
                    for (int j = 2; j < 5; j++)
                    {
                        double cosine = fabs(angle(approx[j%4], approx[j-2], approx[j-1]));
                        maxCosine = MAX(maxCosine, cosine);
                    }
                    
                    if (maxCosine < 0.3)
                        rectangles.push_back(approx);
                }
            }
        }
    }
}

void getlargestRectangle(const std::vector<std::vector<cv::Point> >& rectangles, std::vector<cv::Point>& largestRectangle)
{
    if (!rectangles.size())
    {
        return;
    }
    
    double maxArea = 0;
    int index = 0;
    
    for (size_t i = 0; i < rectangles.size(); i++)
    {
        cv::Rect rectangle = boundingRect(cv::Mat(rectangles[i]));
        double area = rectangle.width * rectangle.height;
        
        if (maxArea < area)
        {
            maxArea = area;
            index = i;
        }
    }
    
    largestRectangle = rectangles[index];
}


double angle( cv::Point pt1, cv::Point pt2, cv::Point pt0 ) {
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}


+(UIImage *) getTransformedImage: (CGFloat) newWidth : (CGFloat) newHeight : (UIImage *) origImage : (CGPoint []) corners : (CGSize) size {
    
  //  cv::Mat imageMat;
    
    
   // CGColorSpaceRef colorSpace = CGImageGetColorSpace(origImage.CGImage);
    CGFloat cols = size.width;
    CGFloat rows = size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC3); // 8 bits per component, 4 channels
    
  /** CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to backing data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
  //  CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), origImage.CGImage);
   // CGContextRelease(contextRef);**/
    UIImageToMat(origImage, cvMat);
 //   imageMat = cvMat;
    
    cv::Mat newImageMat = cv::Mat::zeros(newWidth,newHeight, CV_8UC3);
    
    cv::Point2f src[4], dst[4];
    src[0].x = corners[0].x;
    src[0].y = corners[0].y;
    src[1].x = corners[1].x;
    src[1].y = corners[1].y;
    src[2].x = corners[2].x;
    src[2].y = corners[2].y;
    src[3].x = corners[3].x;
    src[3].y = corners[3].y;
    
    dst[0].x = 0;
    dst[0].y = 0;
    dst[1].x = newWidth;
    dst[1].y = 0;
    dst[2].x = newWidth;
    dst[2].y = newHeight;
    dst[3].x = 0;
    dst[3].y = newHeight;
    
    cv::Mat perspective = cv::getPerspectiveTransform(src, dst);
    cv::warpPerspective(cvMat, newImageMat,perspective , cvSize(newWidth, newHeight));
    
    //Transform to UIImage
    
 /**   NSData *data = [NSData dataWithBytes:newImageMat.data length:newImageMat.elemSize() * newImageMat.total()];
    
    CGColorSpaceRef colorSpace2;
    
    if (newImageMat.elemSize() == 1) {
        colorSpace2 = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace2 = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    CGFloat width = newImageMat.cols;
    CGFloat height = newImageMat.rows;
    
    CGImageRef imageRef = CGImageCreate(width,                                     // Width
                                        height,                                     // Height
                                        8,                                              // Bits per component
                                        8 * newImageMat.elemSize(),                           // Bits per pixel
                                        newImageMat.step[0],                                  // Bytes per row
                                        colorSpace2,                                     // Colorspace
                                        kCGImageAlphaNone | kCGBitmapByteOrderDefault,  // Bitmap info flags
                                        provider,                                       // CGDataProviderRef
                                        NULL,                                           // Decode
                                        false,                                          // Should interpolate
                                        kCGRenderingIntentDefault);                     // Intent
    
    UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace2);**/
    
    return MatToUIImage(newImageMat);
}


@end
