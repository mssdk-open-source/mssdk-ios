//
//  DeviceImagerPicker.swift
//  MobScannerSDK
//
//  Created by Bharat H on 02/01/21.
//  Copyright © 2021 TechVariable. All rights reserved.
//

import UIKit
import Photos

@objc public final class DeviceImagerPicker: TatsiPickerViewController,TatsiPickerViewControllerDelegate {
    
    public var inputImagePickerDelegate: InputImagePickerDelegate? = nil
    
    public init(numberOfSelection: Int) {
        
        var config = TatsiConfig.default
        config.showCameraOption = true
        config.supportedMediaTypes = [.image]
        config.firstView = .userLibrary
        config.colors = TatsiSDKColor()
        config.maxNumberOfSelections = numberOfSelection
        super.init(config: config)
        super.pickerDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func processImages(assets: [PHAsset]){
        
        self.view.showBlurLoader()
        DispatchQueue.background(background: { ()->([String]) in
           
            let group = DispatchGroup()
            var imgFiles = [String]()
            for asset in assets{
                
                group.enter() // wait
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                option.isSynchronous = true
                option.isNetworkAccessAllowed = true

                var assetImage: UIImage!
                
                manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option) { (image, nil) in
                    if let image = image {
                        assetImage = image
                        let path =  Utils.saveCameraImage(image: assetImage)
                        imgFiles.append(path!)
                        group.leave()
                    }
                }
                if assetImage == nil {
                    manager.requestImageData(for: asset, options: option, resultHandler: { (data, _, orientation, _) in
                        if let data = data {
                            if let image = UIImage.init(data: data) {
                                assetImage = image
                                let path =  Utils.saveCameraImage(image: assetImage)
                                imgFiles.append(path!)
                                group.leave()
                            }
                        }
                    })
                }
            }
            
            return imgFiles
        }, completion: { (images) ->() in
            self.view.removeBluerLoader()
            self.dismiss(animated: true, completion: nil)
            self.inputImagePickerDelegate?.onInputImagePicked(images: images!)
        })
    }
    
    public func pickerViewController(_ pickerViewController: TatsiPickerViewController, didPickAssets assets: [PHAsset]) {
       
        processImages(assets: assets)
    }

    private struct TatsiSDKColor: TatsiColors{
        public var background: UIColor = {
            if #available(iOS 13.0, *) {
                return .systemBackground
            } else {
                return .white
            }
        }()

        public var link: UIColor = {
            return MobScannerSDK.INSTANCE.colors.primaryColor
        }()

        public let label: UIColor = {
            return MobScannerSDK.INSTANCE.colors.label
        }()

        public let secondaryLabel: UIColor = {
            if #available(iOS 13.0, *) {
                return .secondaryLabel
            } else {
                return .gray
            }
        }()
        
    }
}
