//
//  ImageFilterViewController.swift
//  MobScanner
//
//  Created by Bharat H on 16/12/20.
//  Copyright © 2020 TechVariable. All rights reserved.
//

import UIKit

protocol ImageFilterCompletionDelegate {
    
    func onFilterApplyDone(image: UIImage)
}

@objc public class ImageFilterViewController: BaseViewController,FilterOptionDelegate {
    
    public func onFilterSelected(filterName: String, filteredImage: UIImage) {
        self.filterView.image = filteredImage
    }
    
    public func getOriginalImage() -> UIImage {
        return croppedImage
    }

    @IBAction func onBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onDoneClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        delegate?.onFilterApplyDone(image: self.filterView.image!)
    }
    
    private var list: FilterOptionList!
    private var filterView: UIImageView!
    public var croppedImage: UIImage = UIImage()
    var delegate: ImageFilterCompletionDelegate? = nil
    public override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        let window = UIApplication.shared.windows[0]
      //  let topSafeArea = window.safeAreaInsets.top
        let bottomPadding = window.safeAreaInsets.bottom + (self.tabBarController?.tabBar.frame.size.height ?? 0)
        let listHeight = CGFloat(100)
        let navHeight = CGFloat(44)
      //  let topPadding = topSafeArea + navHeight
        
        //Nav header
        let navBar = UINavigationBar()
        navBar.barTintColor = MobScannerSDK.INSTANCE.colors.navBackgroundColor
        view.addSubview(navBar)

        let navItem = UINavigationItem(title: "")
        let doneItem = UIBarButtonItem(barButtonSystemItem: .save, target: nil, action: #selector(self.onDoneClicked(_:)))
        doneItem.tintColor = MobScannerSDK.INSTANCE.colors.primaryColor
        let backBtn = Utils.createNavBackButton()
        backBtn.addTarget(self, action: #selector(self.onBackPressed(_:)), for: .touchUpInside)
        
        navItem.leftBarButtonItem = UIBarButtonItem(customView: backBtn)
        
        navItem.rightBarButtonItem = doneItem
        navBar.setItems([navItem], animated: false)
        
        //Custom Views
        list = FilterOptionList(frame: CGRect(x: 0,y: screenHeight - listHeight - bottomPadding,width: screenWidth,height: listHeight))
        filterView = UIImageView()
        filterView.contentMode = .scaleAspectFit
        
        self.view.addSubview(filterView)
        self.view.addSubview(list)
     
        navBar.translatesAutoresizingMaskIntoConstraints = false
        filterView.translatesAutoresizingMaskIntoConstraints = false
        list.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
        
            navBar.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            navBar.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            navBar.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            navBar.heightAnchor.constraint(equalToConstant: navHeight),
            
        
            list.heightAnchor.constraint(equalToConstant: listHeight),
            list.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            list.widthAnchor.constraint(equalTo: self.view.widthAnchor),
            
            filterView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            filterView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            filterView.topAnchor.constraint(equalTo: navBar.bottomAnchor),
            filterView.bottomAnchor.constraint(equalTo: list.topAnchor,constant: 2)
        ]
        
        NSLayoutConstraint.activate(constraints)
        list.setUp(delegate: self)
        
    }
}
