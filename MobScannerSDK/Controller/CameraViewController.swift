//
//  CameraViewController.swift
//  MobScanner
//
//  Created by TechVariable on 31/12/20.
//  Copyright © 2020 TechVariable. All rights reserved.
//

import UIKit

@objc public class CameraViewController: BaseViewController, CameraDelegate, PreviewDelegate {
    
    func onOKClicked(image: UIImage?) {
        
        if(image != nil){
            saveImage(image: image!)
        }
    }
    
    func onRetryClicked() {
        
    }
    
    public var currentFlashMode: FlashModes = .auto
    private let flashBtn = UIButton(type: .custom)
    private let bundle = Bundle(for: CameraViewController.self)
    let countLabel = UILabel()
    public var showCapture = false
    public var isSingleShot = false
    
    public func onImageCaptured(image: UIImage?) {
        
        if(image != nil){
            
            if(showCapture){
                let imagePreviewViewController = ImagePreviewViewController()
                imagePreviewViewController.previewImage = image
                imagePreviewViewController.previewDelegate = self
                self.present(imagePreviewViewController, animated: true, completion: nil)
            }else{
                
                saveImage(image: image!)
            }
            
        }
    }
    
    private func saveImage(image: UIImage){
        
        let path =  Utils.saveCameraImage(image: image)
        imgFiles.append(path!)
        imgCount += 1
        countLabel.text = String(imgCount)
        
        if(isSingleShot){
            self.navigationController?.popViewController(animated: true)
            inputImagePickerDelegate?.onInputImagePicked(images: imgFiles)
        }
    }
    
    public func onError(errorMessage: String) {
        print(errorMessage)
    }
    
    public var inputImagePickerDelegate: InputImagePickerDelegate? = nil
    private var imgCount = 0
    private var imgFiles = [String]()
    private var cameraView: CameraView? = nil
  
    @IBAction func onBackPressed(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onDonePressed(_ sender: UIButton) {
        
        if(imgCount == 0){return}
         
        self.navigationController?.popViewController(animated: true)
        inputImagePickerDelegate?.onInputImagePicked(images: imgFiles)
        
    }
    
    @IBAction func captureClicked(_ sender: UIButton) {
        cameraView?.capture()
    }
    
    @IBAction func flashClicked(_ sender: UIButton) {
        
        switch currentFlashMode {
        case .auto:
            currentFlashMode = .on
            break
        case .on:
            currentFlashMode = .off
            break
        case .off:
            currentFlashMode = .auto
            break
        }
        
        setFlashMode()
    }
    
    private func setFlashMode(){
        
        var flashBtnImg: UIImage? = nil
        
        switch currentFlashMode {
        case .auto:
            flashBtnImg = UIImage(named: "flash_auto.png",in: bundle, compatibleWith: nil)
            break
        case .on:
            flashBtnImg = UIImage(named: "flash_on.png",in: bundle, compatibleWith: nil)
            break
        case .off:
            flashBtnImg = UIImage(named: "flash_off.png",in: bundle, compatibleWith: nil)
            break
        }
        
        cameraView?.updateFlashMode(flasMode: currentFlashMode)
        flashBtn.setImage(flashBtnImg!, for: .normal)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        let window = UIApplication.shared.windows[0]
        var topSafeArea = CGFloat(0.0)
        if #available(iOS 11.0, *) {
            topSafeArea = window.safeAreaInsets.top
        } else {
            // Fallback on earlier versions
            topSafeArea = topLayoutGuide.length
        }
        let bottomSafeArea = window.safeAreaInsets.bottom + (self.tabBarController?.tabBar.frame.size.height ?? 0)
        let viewHeight = screenHeight - topSafeArea - bottomSafeArea
        
        let backBtn = Utils.createNavBackButton(showBackText: false, tintColor: UIColor.white)
        let captureBtn = UIButton(type: .custom)
        let proceedButton = UIButton(type: .roundedRect)
       
        if #available(iOS 13.0, *) {
            let config = UIImage.SymbolConfiguration(pointSize: 20.0, weight: .medium, scale: .medium)
          
            proceedButton.setImage(UIImage(systemName: "chevron.right", withConfiguration: config)?.withTintColor(UIColor.black), for: .normal)
            captureBtn.setImage(UIImage(named:"camera.png",in: bundle,compatibleWith: nil)?.withTintColor(UIColor.white), for: .normal)
            
        } else {
            
            captureBtn.setImage(UIImage(named:"camera.png",in: bundle,compatibleWith: nil)?.withRenderingMode(.alwaysTemplate), for: .normal)
            captureBtn.tintColor = UIColor.white
            
            proceedButton.setImage(UIImage(named:"chevron-right.png",in: bundle,compatibleWith: nil), for: .normal)
            proceedButton.tintColor = UIColor.black
            
        }
       
        backBtn.addTarget(self, action: #selector(self.onBackPressed(_:)), for: .touchUpInside)
      
        cameraView = CameraView(frame: CGRect(x: 0,y: topSafeArea,width: screenWidth,height: viewHeight))
     
        cameraView?.backgroundColor = UIColor.black
        
        let captureImgHeight = CGFloat(54)
        let captureImgWidth = CGFloat(72)
    
        captureBtn.addTarget(self, action: #selector(self.captureClicked(_:)), for: .touchUpInside)
        
        setFlashMode()
        
        flashBtn.tintColor = UIColor.white
        flashBtn.addTarget(self, action: #selector(self.flashClicked(_:)), for: .touchUpInside)
      
        
        let proceedBtnHW = CGFloat(40)
        
        proceedButton.backgroundColor = UIColor.white
        
        proceedButton.tintColor = UIColor.black
        proceedButton.layer.cornerRadius = proceedBtnHW/2
        proceedButton.addTarget(self, action: #selector(self.onDonePressed(_:)), for: .touchUpInside)
        
        let countLblHW = CGFloat(22)
       
        countLabel.layer.cornerRadius = countLblHW/2
        countLabel.text = "0"
        countLabel.font = .systemFont(ofSize: 14)
        countLabel.layer.masksToBounds = true
        countLabel.textColor = UIColor.white
        countLabel.textAlignment = .center
        
        countLabel.backgroundColor = MobScannerSDK.INSTANCE.colors.primaryColor
        
        self.view.addSubview(cameraView!)
        self.view.addSubview(backBtn)
        self.view.addSubview(captureBtn)
        self.view.addSubview(flashBtn)
        self.view.addSubview(proceedButton)
        self.view.addSubview(countLabel)
        
        cameraView!.translatesAutoresizingMaskIntoConstraints = false
        flashBtn.translatesAutoresizingMaskIntoConstraints = false
        captureBtn.translatesAutoresizingMaskIntoConstraints = false
        proceedButton.translatesAutoresizingMaskIntoConstraints = false
        backBtn.translatesAutoresizingMaskIntoConstraints = false
        countLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
        
            cameraView!.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            cameraView!.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            cameraView!.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            cameraView!.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            
            
            flashBtn.widthAnchor.constraint(equalToConstant: 20),
            flashBtn.heightAnchor.constraint(equalToConstant: 20),
            flashBtn.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor,constant: 10),
            flashBtn.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor,constant: -10),
            
            captureBtn.widthAnchor.constraint(equalToConstant: captureImgWidth),
            captureBtn.heightAnchor.constraint(equalToConstant: captureImgHeight),
            captureBtn.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -(captureImgHeight/2)),
            captureBtn.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            
            backBtn.widthAnchor.constraint(equalToConstant: 20),
            backBtn.widthAnchor.constraint(equalToConstant: 20),
            backBtn.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor,constant: 10),
            backBtn.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor,constant: 10),
            
            proceedButton.widthAnchor.constraint(equalToConstant: proceedBtnHW),
            proceedButton.heightAnchor.constraint(equalToConstant: proceedBtnHW),
            proceedButton.topAnchor.constraint(equalTo: captureBtn.topAnchor,constant: (captureImgHeight-proceedBtnHW)/2),
            proceedButton.leftAnchor.constraint(equalTo: captureBtn.rightAnchor, constant: (screenWidth/4)-(proceedBtnHW)),
            
            countLabel.widthAnchor.constraint(equalToConstant: countLblHW),
            countLabel.heightAnchor.constraint(equalToConstant: countLblHW),
            countLabel.topAnchor.constraint(equalTo: proceedButton.topAnchor,constant: 0),
            countLabel.leadingAnchor.constraint(equalTo: proceedButton.trailingAnchor, constant: -countLblHW/2)
        ]
        
        NSLayoutConstraint.activate(constraints)
        
        cameraView?.startCamera(delegate: self)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        cameraView?.stopCamera()
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        cameraView?.onResize(newSize: size)
    }
}
