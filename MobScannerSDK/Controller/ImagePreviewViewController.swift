//
//  ImagePreviewViewController.swift
//  MobScannerSDK
//
//  Created by TechVariable on 04/01/21.
//  Copyright © 2021 TechVariable. All rights reserved.
//

import UIKit

protocol PreviewDelegate {
    func onOKClicked(image: UIImage?)
    func onRetryClicked()
}

@objc public class ImagePreviewViewController: BaseViewController {

    public var previewImage: UIImage? = nil
    var previewDelegate: PreviewDelegate? = nil
    
    @IBAction func onOkClicked(_ sender: UIButton) {
        
        previewDelegate?.onOKClicked(image: previewImage)
        self.dismiss(animated: true, completion: nil)
       }
    @IBAction func onRetryClicked(_ sender: UIButton) {
          
        self.dismiss(animated: true, completion: nil)
       }
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()

        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        let window = UIApplication.shared.windows[0]
        let topSafeArea = window.safeAreaInsets.top
        let navHeight = CGFloat(50)
        let topPadding = topSafeArea + navHeight
        
        //Nav header
        let navBar = UINavigationBar()
        view.addSubview(navBar)
        
        let navItem = UINavigationItem(title: "Preview")
        
        let okItem = UIBarButtonItem(title: "OK", style: UIBarButtonItem.Style.plain, target: nil, action: #selector(self.onOkClicked(_:)))
        okItem.tintColor = MobScannerSDK.INSTANCE.colors.primaryColor
        
        let retryItem = UIBarButtonItem(title: "Retry", style: UIBarButtonItem.Style.plain, target: nil, action: #selector(self.onRetryClicked(_:)))
        retryItem.tintColor = MobScannerSDK.INSTANCE.colors.primaryColor
        
        navItem.leftBarButtonItems = [retryItem]
        
        navItem.rightBarButtonItems = [okItem]
        navBar.setItems([navItem], animated: false)
        
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
     //   imageView.frame = CGRect(x: 0, y: navHeight, width: screenWidth, height: screenHeight-navHeight-topPadding)
        imageView.image = previewImage
       
        self.view.addSubview(imageView)
        self.view.layer.cornerRadius = 10
        self.view.layer.masksToBounds = true
        
        self.view.backgroundColor = MobScannerSDK.INSTANCE.colors.navBackgroundColor

        navBar.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
        
            navBar.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            navBar.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            navBar.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            navBar.heightAnchor.constraint(equalToConstant: navHeight),
            
    
            imageView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            imageView.topAnchor.constraint(equalTo: navBar.bottomAnchor),
            imageView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
