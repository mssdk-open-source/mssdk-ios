//
//  CropViewController.swift
//  MobScanner
//
//  Created by Bharat H on 15/12/20.
//  Copyright © 2020 TechVariable. All rights reserved.
//

import UIKit


@objc public class CropViewController: BaseViewController, ImageFilterCompletionDelegate {
    
    func onFilterApplyDone(image: UIImage) {
        let folder = Utils.getFolder(folderName: getOutputFolderName())
        let outputFilePath = Utils.saveImage(folder: folder!, image: image)
        outputImgFiles.append(outputFilePath!)
        fileIndex += 1
        loadImage()
    }
    
    private func getOutputFolderName()->String{
        outputFolder = outputFolder ?? Utils.getScanFolderName()
        return outputFolder!
    }
    
    private var cropView: CropView!
    public var outputFolder: String? = nil
    public var imgFiles = [String]()
    public var outputImageTargetSize: CGSize? = nil
    public var outputImageQuality: CGFloat = 0.9
    private var outputImgFiles = [String]()
    private var fileIndex = 0
    public var documentReadyDelegate: DocumentReadyDelegate? = nil
    private let countLabel = UILabel()
    
    
    @IBAction func onBackPressed(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "", message: "Do you want to exit without saving image?", preferredStyle: UIAlertController.Style.alert)

        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
              
            Utils.clearTempFolder()
            self.navigationController?.popViewController(animated: true)
            self.documentReadyDelegate?.onUserCancelled()

        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))

        // show the alert
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showCropErrorDialog(){
        
        let alert = UIAlertController(title: "", message: "Error in croping. Please try again", preferredStyle: UIAlertController.Style.alert)

       
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onRotatePressed(_ sender: UIButton) {
        
        cropView.rotateImage(angle: 90)
    }
    
    @IBAction func onDoneClicked(_ sender: UIButton) {
        
        cropView.cropAndTransform(completionHandler: {(croppedImage) -> Void in
            
            if(croppedImage != nil){
                let imageVC = self.storyboard?.instantiateViewController(withIdentifier: "ImageFilterViewController") as! ImageFilterViewController
                imageVC.croppedImage = croppedImage!
                //print(croppedImage.size.width,croppedImage.size.height)
                imageVC.delegate = self
                self.navigationController?.pushViewController(imageVC, animated: true)
            }else{
                self.showCropErrorDialog()
            }
        })
    }
    
    private func loadImage(){
        
        if(imgFiles.count > fileIndex){
            let file = imgFiles[fileIndex]
            let image = UIImage(contentsOfFile: file)
            if(image != nil){
                showSpinner(onView: self.view)
                cropView.isHidden = true
                if(outputImageTargetSize == nil){
                    
                    let bounds = UIScreen.main.bounds
                    let width = bounds.size.width
                    let height = bounds.size.height
                    
                    outputImageTargetSize = CGSize(width: width, height: height)
                }
                
                DispatchQueue.background(background: { ()->(UIImage?) in
                    
                    return image!.resize(targetSize: self.outputImageTargetSize!, targetQuality: self.outputImageQuality)
                    
                    
                }, completion: { (image) ->() in
                    
                    self.removeSpinner()
                    self.cropView.isHidden = false
                    self.cropView.setUpImage(image: image!)
                    
                })
                
                
                self.countLabel.text = String(fileIndex+1)+" of "+String(imgFiles.count)
                
            }
        }else{
            
            self.navigationController?.popViewController(animated: true)
            documentReadyDelegate?.onDocumentReady(outputImgFiles: outputImgFiles)
            Utils.clearTempFolder()
            
        }
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
       
        let navHeight = CGFloat(44)
        
        //Nav header
        let navBar = UINavigationBar()
        navBar.barTintColor = MobScannerSDK.INSTANCE.colors.navBackgroundColor
        view.addSubview(navBar)
        
        let navItem = UINavigationItem(title: "")
        let doneItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: nil, action: #selector(self.onDoneClicked(_:)))
        
        let rotateItem = UIBarButtonItem(title: "Rotate", style: UIBarButtonItem.Style.plain, target: nil, action: #selector(self.onRotatePressed(_:)))
        
        rotateItem.tintColor = MobScannerSDK.INSTANCE.colors.primaryColor
        doneItem.tintColor = MobScannerSDK.INSTANCE.colors.primaryColor
        let rightItems = [doneItem,rotateItem]
        
        let backBtn = Utils.createNavBackButton()
        backBtn.addTarget(self, action: #selector(self.onBackPressed(_:)), for: .touchUpInside)
        
        navItem.leftBarButtonItem = UIBarButtonItem(customView: backBtn)
        
        navItem.rightBarButtonItems = rightItems
        navBar.setItems([navItem], animated: false)
        
        self.cropView = CropView()
       
        loadImage()
        
        self.countLabel.textColor = MobScannerSDK.INSTANCE.colors.label
        // self.countLabel.backgroundColor = UIColor.init(argb: 0x90000000)
        self.countLabel.textAlignment = .center
        self.view.addSubview(self.countLabel)
        self.view.addSubview(self.cropView)
        
        navBar.translatesAutoresizingMaskIntoConstraints = false
        cropView.translatesAutoresizingMaskIntoConstraints = false
        countLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
        
            navBar.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            navBar.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            navBar.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            navBar.heightAnchor.constraint(equalToConstant: navHeight),
            
            cropView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            cropView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            cropView.topAnchor.constraint(equalTo: navBar.bottomAnchor),
            cropView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
          
            countLabel.topAnchor.constraint(equalTo: navBar.bottomAnchor, constant: 20),
            countLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
            
        ]
        
        NSLayoutConstraint.activate(constraints)
        
    }

}
